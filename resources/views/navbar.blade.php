<nav>
  <ul class="container" style="display: flex; justify-content: space-between;">
    <li class="logo"><a href="{{ route('home') }}" style="color: white;">Majoo Teknologi Indonesia</a></li>
    <div>
      <a href="{{ route('produk.index') }}" style="font-size: medium;">Produk</a>
      <a href="{{ route('kategori.index') }}" style="font-size: medium;">Kategori</a>
    </div>
  </ul>
</nav>
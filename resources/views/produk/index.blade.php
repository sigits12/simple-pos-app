@extends('master')

@section('content')
<div style="margin: 8px 0 8px 0; display: flex; justify-content: space-between;">
  <h3>{{ $title }}</h3>
  <a class="btn btn-primary" href="{{ route('produk.create') }}">Tambah</a>
</div>
<div class="list-products">
  @foreach($produks as $produk)
  <div class="card">
    <img class="card-img-top" src="{{ asset('storage/'.$produk->foto_produk) }}" alt="produk-image">
    <div class="card-body">
      <h5 class="card-title">{{ $produk->nama }}</h5>
      <h5 class="card-title">{{ $produk->harga }}</h5>
      <p class="card-text">{!! $produk->deskripsi !!}</p>
      <div class="beli-button">
        <a href="#" class="btn btn-primary">Beli</a>
      </div>
    </div>
  </div>
  @endforeach

</div>

@endsection
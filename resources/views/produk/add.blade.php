@extends('master')

@section('content')
<div style="margin: 8px 0 8px 0; display: flex; justify-content: space-between;">
  <h3>{{ $title }}</h3>
</div>
<div class="row">
  <div class="col-lg-12">
    <form role="form" method="POST" action="{{ route('produk.store') }}" enctype="multipart/form-data">
      @csrf

      <div class="form-group">
        <label for="nama">Nama</label>
        <input name="nama" class="form-control" id="nama" value="{{ old('nama') }}" aria-describedby="namaError">
        @error('nama')
          <small id="namaError" class="form-text text-danger">* {{ $message }}</small>
        @enderror
      </div>

      <div class="form-group">
        <label for="deskripsi">Deskripsi</label>
        <textarea name="deskripsi" id="deskripsi" class="form-control" rows="3" aria-describedby="deskripsiError" value="{{ old('deskripsi') }}"></textarea>
        @error('deskripsi')
          <small id="deskripsiError" class="form-text text-danger">* {{ $message }}</small>
        @enderror
      </div>

      <div class="form-group">
        <label for="harga">Harga</label>
        <input name="harga" class="form-control" id="harga" aria-describedby="@error('harga') hargaError @enderror hargaHelp"  value="{{ old('harga') }}" />
        <small id="hargaHelp" class="form-text text-muted">Misal: 120000</small>
        @error('harga')
          <small id="hargaError" class="form-text text-danger">* {{ $message }}</small>
        @enderror
      </div>

      <div class="form-group">
        <select class="form-control select-kategori" name="kategori_id" aria-describedby="kategoriError">
          @foreach($kategori as $data)
          <option value="{{ $data->id }}">{{ $data->nama }}</option>
          @endforeach
        </select>
        @error('kategori_id')
          <small id="kategoriError" class="form-text text-danger">* {{ $message }}</small>
        @enderror
      </div>

      <div class="form-group">
        <label for="gambarProduk">Gambar Produk</label>
        <input class="form-control" id="gambarProduk" type='file' onchange="readURL(this);" name="image" aria-describedby="imageError/>
        <img style="margin-top: 10px;" id="preview" src="#" alt="gambar produk" />
        @error('image')
          <small id="imageError" class="form-text text-danger">* {{ $message }}</small>
        @enderror
      </div>

      <div style="margin-top: 10px; display: flex; justify-content: space-between;">
        <div>
          <button type="submit" class="btn btn-primary">Simpan</button>
          <button type="reset" class="btn btn-secondary">Reset</button>
        </div>
        <a class="btn btn-warning" href="{{ route('produk.index') }}">Batal</a>
      </div>

    </form>
  </div>

</div>

@endsection

@push('scripts')
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
<script src="{{ asset('js/select2.min.js') }}"></script>
<script src="{{ asset('js/ckeditor.js') }}"></script>
<script src="{{ asset('js/reviewImage.js') }}"></script>

<script>
  $(document).ready(function() {
    $('.select-kategori').select2();

    ClassicEditor
      .create(document.querySelector('#deskripsi'), {
        toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ],
      })
      .catch(error => {
        console.error(error);
      });

  });
</script>
@endpush
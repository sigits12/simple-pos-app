@extends('master')

@section('content')
<div style="margin: 8px 0 8px 0; display: flex; justify-content: space-between;">
  <h3>{{ $title }}</h3>
  <a class="btn btn-primary" href="{{ route('kategori.create') }}">Tambah</a>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Keterangan</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach($kategoris as $key => $kategori)
              <tr>
                <td>{{ $key+1 }}</td>
                <td>{{ $kategori->nama }}</td>
                <td>{{ $kategori->deskripsi }}</td>
                <td>
                  <a href="{{ route('kategori.edit', ['id' => $kategori->id] ) }}">Edit</a>
                  <a class="hapus" href="{{ route('kategori.destroy', ['id' => $kategori->id] ) }}">Hapus</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
        <!-- /.table-responsive -->
      </div>
      <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
  </div>
  <!-- /.col-lg-12 -->
</div>
@push('scripts')
<script>
  
  $(document).ready(function() {
    $('.hapus').click(function(event) {
      event.preventDefault();

      var me = $(this),
        url = me.attr('href'),
        // url = "#",
        csrf_token = $('meta[name="csrf-token"]').attr('content');

        swal({
        title: 'Apakah anda yakin ingin menghapus data ini ?',
        icon: "warning",
        buttons: ['Batal', "Ya, Hapus!"],
      }).then((result) => {
        if (result) {
          $.ajax({
            url: url,
            type: "POST",
            data: {
              '_method': 'DELETE',
              '_token': csrf_token
            },
            success: function(response) {
              swal({
                icon: 'success',
                title: 'Sukses!',
                text: 'Kategori berhasil dihapus!'
              });
              setTimeout(function() {
                location.reload();
              }, 1000);
            },
            error: function(xhr) {
              swal({
                icon: 'error',
                title: 'Oops...',
                text: 'Ada yang salah proses hapus!'
              });
            }
          });
        }
      });
    });
  });
</script>
@endpush
@endsection
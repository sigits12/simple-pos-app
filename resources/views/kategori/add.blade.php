@extends('master')

@section('content')
<div style="margin: 8px 0 8px 0; display: flex; justify-content: space-between;">
  <h3>{{ $title }}</h3>
</div>
<div class="row">
  <div class="col-lg-12">
    <form role="form" 
      method="POST" 
      action="@if($kategori->id){{ route('kategori.update', ['id' => $kategori->id]) }}@else{{ route('kategori.store') }}@endif">
      @if($kategori->id)
      {{ method_field('PUT') }}
      @endif
      @csrf
      <div class="form-group">
        <label for="nama">Nama *</label>
        <input name="nama" class="form-control" id="nama" value="{{ old('nama', $kategori->nama) }}" aria-describedby="namaError">
        @error('nama')
        <small id="namaError" class="form-text text-danger">* {{ $message }}</small>
        @enderror
      </div>

      <div class="form-group">
        <label for="deskripsi">Deskripsi *</label>
        <textarea name="deskripsi" id="deskripsi" class="form-control" rows="3" aria-describedby="deskripsiError"">{{ old('deskripsi', $kategori->deskripsi) }}</textarea>
        @error('deskripsi')
        <small id="deskripsiError" class="form-text text-danger">* {{ $message }}</small>
        @enderror
      </div>

      <div style="margin-top: 10px; display: flex; justify-content: space-between;">
        <div>
          <button type="submit" class="btn btn-primary">Simpan</button>
          <button type="reset" class="btn btn-secondary">Reset</button>
        </div>
        <a class="btn btn-warning" href="{{ route('kategori.index') }}">Batal</a>
      </div>

    </form>
  </div>

</div>

@endsection
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>POS APP</title>

  <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap4.6/bootstrap.min.css') }}">  
  <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">

  <script src="{{ asset('js/bootstrap4.6/jquery-3.6.0.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap4.6/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap4.6/popper.min.js') }}"></script>
  <script src="{{ asset('js/sweetalert.min.js') }}"></script>
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  @stack('scripts')
</head>

<body>
  @include('sweet::alert')
  @include('navbar')
  <div class="container">
    @yield('content')
  </div>

</body>

</html>
<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\KategoriController;

Route::get('', function (){
    return redirect()->route('produk.index');
})->name('home');

Route::group(['prefix' => 'produk'], function () {
    Route::get('', [ProdukController::class, 'index'])->name('produk.index');
    Route::post('', [ProdukController::class, 'store'])->name('produk.store');
    Route::get('/add', [ProdukController::class, 'create'])->name('produk.create');
});

Route::group(['prefix' => 'kategori'], function () {
    Route::get('', [KategoriController::class, 'index'])->name('kategori.index');
    Route::get('/add', [KategoriController::class, 'create'])->name('kategori.create');
    Route::post('', [KategoriController::class, 'store'])->name('kategori.store');
    Route::get('/{id}', [KategoriController::class, 'show'])->name('kategori.show');
    Route::get('/edit/{id}', [KategoriController::class, 'edit'])->name('kategori.edit');
    Route::put('/update/{id}', [KategoriController::class, 'update'])->name('kategori.update');
    Route::delete('/delete/{id}', [KategoriController::class, 'destroy'])->name('kategori.destroy');
});
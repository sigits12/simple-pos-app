<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class Kategori extends ResourceCollection
{
    public function toArray($resource)
    {
        return parent::toArray($resource);
    }
}

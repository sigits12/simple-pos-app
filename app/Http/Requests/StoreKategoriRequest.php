<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreKategoriRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if($this->method() == "PUT") {
            return [
                'nama' => 'required|max:255|unique:kategori,nama,'.$this->id,
                'deskripsi' => 'required',
            ];
        } else {
            return [
                'nama' => 'required|max:255|unique:kategori,nama',
                'deskripsi' => 'required',
            ];
        }
    }

    public function messages()
    {
        return [
            'nama.required' => "Nama tidak boleh kosong.",
            'nama.max' => "Nama tidak boleh lebih dari 255 karakter.",
            'nama.unique' => "Kategori dengan nama tersebut sudah ada.",
            'deskripsi.required' => "Deskripsi tidak boleh kosong.",
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class StoreProdukRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nama' => 'required|max:255|unique:produk,nama',
            'deskripsi' => 'required',
            'harga' => 'required|integer',
            'kategori_id' => 'required|exists:kategori,id',
            'image' => 'required|mimes:jpeg,jpg,png|max:50000',
        ];
    }

    public function messages()
    {
        return [
            'nama.required' => "Nama tidak boleh kosong.",
            'nama.max' => "Nama tidak boleh lebih dari 255 karakter.",
            'nama.unique' => "Produk dengan nama tersebut sudah ada.",
            'deskripsi.required' => "Deskripsi tidak boleh kosong.",
            'harga.required' => "Harga tidak boleh kosong.",
            'harga.integer' => "Harga harus angka.",
            'kategori_id.required' => "Kategori tidak boleh kosong.",
            'kategori_id.exist' => "Kategori harus dari pilihan.",
            'image.required' => "Gambar produk tidak boleh kosong.",
            'image.mimes' => "Gambar produk harus bertipe jpeg, jpg atau png.",
        ];
    }

    public function withValidator($validator)
    {
        $validator->after(function ($validator) {
            $this->saveUploadedFiles($validator);
        });
    }

    private function getUploadedFilename($field, $file)
    {
        return $field . '-' . strtolower(str_replace(' ', '_', $this->nama)) . '.' . $file->getClientOriginalExtension();
    }


    private function saveUploadedFiles($validator)
    {
        $field = 'foto_produk';
        $form_field = 'image';

        $file = $this->file('image');
        if ($file) {
            $file_name = $this->getUploadedFilename($field, $file);
            $stored = Storage::putFileAs('produk',$file, $file_name);
            if ($stored) {
                $this->merge([
                    $field => $stored,
                ]);
            } else {
                $validator->errors()->add(
                    $form_field,
                    "Tidak berhasil mengupload gambar."
                );
            }
        }
    }
}

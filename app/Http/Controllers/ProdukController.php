<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProdukRequest;
use App\Models\Kategori;
use App\Models\Produk;

class ProdukController extends Controller
{
    public function index()
    {
        $produks = Produk::get();

        return view('produk.index')
            ->with('produks', $produks)
            ->with('title', 'Produk');
    }

    public function create()
    {
        $kategori = Kategori::get();
        return view('produk.add')
            ->with('kategori', $kategori)
            ->with('title', 'Tambah Produk');
    }

    public function store(StoreProdukRequest $request)
    {
        $object = Produk::create($request->all());

        if($object->id) {
            alert()->success('Produk berhasil ditambah', 'Sukses!')->autoclose(4500);
        } else {
            alert()->error('Produk gagal ditambah', 'Gagal!')->autoclose(4500);
        }

        return redirect()->route('produk.index');
    }
}

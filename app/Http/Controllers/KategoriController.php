<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreKategoriRequest;
use App\Models\Kategori;
use Illuminate\Database\QueryException;

class KategoriController extends Controller
{
    public function index()
    {
        $kategoris = Kategori::get();

        return view('kategori.index')
            ->with('kategoris', $kategoris)
            ->with('title', 'Kategori');
    }

    public function create()
    {
        $kategori = new Kategori();
        return view('kategori.add')
            ->with('kategori', $kategori)
            ->with('title', 'Tambah Kategori');
    }

    public function store(StoreKategoriRequest $request)
    {
        try {
            Kategori::create($request->all());
            alert()->success('Kategori berhasil ditambah', 'Sukses!')->autoclose(4500);
        } catch (QueryException $e) {
            alert()->error('Gagal menambah kategori', 'Gagal!')->autoclose(4500);
        }
        return redirect()->route('kategori.index');
    }

    public function edit($id)
    {
        $kategori = Kategori::find($id);

        return view('kategori.add')
            ->with('kategori', $kategori)
            ->with('title', 'Edit Kategori');
    }

    public function update(StoreKategoriRequest $request, $id)
    {
        try {
            Kategori::find($id)->update($request->all());
            alert()->success('Kategori berhasil diubah', 'Sukses!')->autoclose(4500);
        } catch (QueryException $e) {
            alert()->error('Gagal merubah kategori', 'Gagal!')->autoclose(4500);
        }
        return redirect()->route('kategori.index');
    }

    public function destroy($id)
    {
        try {
            $kategori = Kategori::findOrFail($id);
            return $kategori->delete();
        } catch (QueryException $e) {
            return $e;
        }
    }
}

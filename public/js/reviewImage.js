function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#preview')
                .attr('src', e.target.result)
                .width('auto')
                .height(320);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
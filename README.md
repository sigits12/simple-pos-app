# Simple Pos App

Menggunakan laravel 8 dan postgresql
Cara menggunakan:
1. git clone https://gitlab.com/sigits12/simple-pos-app.git
2. composer install
3. php artisan migrate
4. php artisan db:seed -- KategoriSeeder
5. php artisan serve

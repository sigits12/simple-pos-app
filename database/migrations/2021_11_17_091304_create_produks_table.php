<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('kategori', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->unique('nama', 'unique_nama_kategori');
            $table->text('deskripsi');
            $table->timestamps();
        });

        Schema::create('produk', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->unique('nama', 'unique_nama_produk');
            $table->text('deskripsi');
            $table->integer('harga')->default(0);
            $table->integer('kategori_id');
            $table->string('foto_produk');
            $table->timestamps();

            $table->foreign('kategori_id')->references('id')->on('kategori')->onUpdate('cascade')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produk');
        Schema::dropIfExists('kategori');
    }
}

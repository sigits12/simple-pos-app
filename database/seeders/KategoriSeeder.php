<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Kategori;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'nama' => 'Mesin Terpadu',
                'deskripsi' => 'Mesin kasir dengan mesin print jadi satu',
            ],
            [
                'nama' => 'Mesin Tepadu Stand',
                'deskripsi' => 'Mesin kasir dengan mesin print jadi satu dan memiliki stand',
            ],
            [
                'nama' => 'Mesin Tepisah',
                'deskripsi' => 'Mesin kasir dengan mesin print terpisah',
            ],
        ];
        Kategori::insert($data);
    }
}
